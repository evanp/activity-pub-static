# activity-pub-static

Static ActivityPub data for testing AP clients. Doesn't do any authc/authz; you'll need to add that if you use this data.